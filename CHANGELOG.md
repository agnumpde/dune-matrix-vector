# Master (will become release 2.10)

- ...

## Deprecations and removals

- ...

# 2.9 Release

- No changes compare to the 2.8 release

# 2.8 Release

- The method `addToDiagonal` can now also be called if the matrix is a scalar number type.
  This is needed since nowadays scalar entries can end the nesting recursion of dune-istl
  matrices.
