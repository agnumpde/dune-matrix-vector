#ifndef DUNE_MATRIX_VECTOR_UTILITIES_HH
#define DUNE_MATRIX_VECTOR_UTILITIES_HH

#include <type_traits>

#include <dune/common/std/type_traits.hh>
#include <dune/common/typetraits.hh>

#include <dune/matrix-vector/concepts.hh>
#include <dune/matrix-vector/traits/scalartraits.hh>
#include <dune/matrix-vector/traits/matrixtraits.hh>
#include <dune/matrix-vector/traits/vectortraits.hh>

// coming below to avoid circular dependency:
// #include <dune/matrix-vector/traits/isquadratic.hh>

namespace Dune {
namespace MatrixVector {

// convenience compile-time functions to classify types
template <class T>
constexpr auto isNumber() {
  return Std::bool_constant<IsNumber<std::decay_t<T>>::value>();
}

template <class T>
constexpr auto isScalar() {
  return Std::bool_constant<Traits::ScalarTraits<std::decay_t<T>>::isScalar>();
}

template <class T>
constexpr auto isVector() {
  return Std::bool_constant<Traits::VectorTraits<std::decay_t<T>>::isVector>();
}

template <class T>
constexpr auto isMatrix() {
  return Std::bool_constant<Traits::MatrixTraits<std::decay_t<T>>::isMatrix>();
}

template <class T>
constexpr auto isTupleOrDerived() {
  return Std::bool_constant<IsTupleOrDerived<std::decay_t<T>>::value>();
}

// enable_if typedefs for traits ...
template <class T, class R = void>
using EnableNumber = std::enable_if_t<isNumber<T>(), R>;

template <class T, class R = void>
using EnableScalar = std::enable_if_t<isScalar<T>(), R>;

template <class T, class R = void>
using EnableVector = std::enable_if_t<isVector<T>(), R>;

template <class T, class R = void>
using EnableMatrix = std::enable_if_t<isMatrix<T>(), R>;

template <class T, class R = void>
using EnableTupleOrDerived = std::enable_if_t<isTupleOrDerived<T>(), R>;

// ... and their negations
template <class T, class R = void>
using DisableNumber = std::enable_if_t<not isNumber<T>(), R>;

template <class T, class R = void>
using DisableScalar = std::enable_if_t<not isScalar<T>(), R>;

template <class T, class R = void>
using DisableVector = std::enable_if_t<not isVector<T>(), R>;

template <class T, class R = void>
using DisableMatrix = std::enable_if_t<not isMatrix<T>(), R>;

template <class T, class R = void>
using DisableTupleOrDerived = std::enable_if_t<not isTupleOrDerived<T>(), R>;

// compile-time property checks
template <class T>
constexpr auto isSparseRangeIterable() {
  return Std::bool_constant<isTupleOrDerived<T>() or
                            models<Concept::HasBegin, T>()>();
}

} // end namespace MatrixVector
} // end namespace Dune

#include <dune/matrix-vector/traits/isquadratic.hh>

namespace Dune {
namespace MatrixVector {

template <class T>
constexpr auto isQuadratic() {
  return std::integral_constant<
      int, Traits::IsQuadratic<std::decay_t<T>>::dimension>();
}

} // end namespace MatrixVector
} // end namespace Dune


#endif // DUNE_MATRIX_VECTOR_TRAITS_UTILITIES_HH
