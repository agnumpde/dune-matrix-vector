#ifndef DUNE_MATRIX_VECTOR_TRAITS_VECTORTRAITS_HH
#define DUNE_MATRIX_VECTOR_TRAITS_VECTORTRAITS_HH

#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/multitypeblockvector.hh>

namespace Dune {
namespace MatrixVector {
namespace Traits {

/** \brief Class to identify vector types and extract information
 *
 * Specialize this class for all types that can be used like a vector.
 */
template <class T>
struct VectorTraits {
  constexpr static bool isVector = false;
};

template <class K, int n>
struct VectorTraits<FieldVector<K, n>> {
  constexpr static bool isVector = true;
};

template <class Block>
struct VectorTraits<BlockVector<Block>> {
  constexpr static bool isVector = true;
};

} // end namespace Traits
} // end namespace MatrixVector
} // end namespace Dune

#endif // DUNE_MATRIX_VECTOR_TRAITS_VECTORTRAITS_HH
