#ifndef DUNE_MATRIX_VECTOR_TRAITS_FIELDTRAITS_HH
#define DUNE_MATRIX_VECTOR_TRAITS_FIELDTRAITS_HH

#include <dune/common/typetraits.hh>
#include <dune/istl/scaledidmatrix.hh>

/**
 * \file
 * Add FieldTraits that are not in the core modules.
 */

namespace Dune {

template <class K, int n>
struct FieldTraits<ScaledIdentityMatrix<K, n>> {
  using field_type = field_t<K>;
  using real_type = real_t<K>;
};

} // end namespace Dune

#endif // DUNE_MATRIX_VECTOR_TRAITS_FIELDTRAITS_HH
