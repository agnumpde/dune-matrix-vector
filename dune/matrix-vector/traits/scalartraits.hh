#ifndef DUNE_MATRIX_VECTOR_TRAITS_SCALARTRAITS_HH
#define DUNE_MATRIX_VECTOR_TRAITS_SCALARTRAITS_HH

#include <dune/common/diagonalmatrix.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/typetraits.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/scaledidmatrix.hh>

namespace Dune {
namespace MatrixVector {
namespace Traits {

/** \brief Class to identify scalar types
 *
 * Specialize this class for all types that can be used
 * like scalar quantities.
 */
template <class T>
struct ScalarTraits {
  constexpr static bool isScalar = Dune::IsNumber<T>::value;
};

template <class T>
struct ScalarTraits<Dune::FieldVector<T, 1>> {
  constexpr static bool isScalar = true;
};

template <class T>
struct ScalarTraits<Dune::FieldMatrix<T, 1, 1>> {
  constexpr static bool isScalar = true;
};

template <class T>
struct ScalarTraits<Dune::DiagonalMatrix<T, 1>> {
  constexpr static bool isScalar = true;
};

template <class T>
struct ScalarTraits<Dune::ScaledIdentityMatrix<T, 1>> {
  constexpr static bool isScalar = true;
};

} // end namespace Traits
} // end namespace MatrixVector
} // end namespace Dune

#endif // DUNE_MATRIX_VECTOR_TRAITS_SCALARTRAITS_HH
