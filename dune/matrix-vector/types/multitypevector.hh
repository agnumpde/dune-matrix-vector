﻿#ifndef DUNE_MATRIX_VECTOR_TYPES_MULTITYPEVECTOR_HH
#define DUNE_MATRIX_VECTOR_TYPES_MULTITYPEVECTOR_HH

#include <dune/common/typetraits.hh>
#include <dune/istl/multitypeblockvector.hh>
#include <dune/matrix-vector/traits/utilities.hh>
#include <dune/matrix-vector/traits/vectortraits.hh>

namespace Dune {
namespace MatrixVector {

/**
 * \brief Is a dune-istl MultiTypeBlockVector with additional functionality:
 *   - is nestable in other dune matrix types, e.g. BlockVector
 *   - construction from scalar
 *   - scaling through division by scalar (multiplication is available)
 */
template <class... Args>
struct MultiTypeVector : public MultiTypeBlockVector<Args...> {
  using Base = MultiTypeBlockVector<Args...>;
  using Base::operator=;

  // Fake value to silence vectors that extract this parameter.
  constexpr static int blocklevel = -1000;

  /** \brief Default constructor */
  MultiTypeVector() {}

  /** \brief Constructor initializing all blocks with given scalar */
  template <class K, typename = EnableNumber<K>>
  MultiTypeVector(K value) {
    *this = value;
  }

  template <class K, typename = EnableNumber<K>>
  void operator/=(const K& w) {
    Dune::Hybrid::forEach(*this, [&](auto&& entry) { entry /= w; });
  }
};

// inject vector traits
namespace Traits {

template <class... Args>
struct VectorTraits<MultiTypeVector<Args...>> {
  constexpr static bool isVector = true;
};

} // end namespace Traits

} // end namespace MatrixVector

// inject field traits from vector
template <class Arg, class... Args>
struct FieldTraits<MatrixVector::MultiTypeVector<Arg, Args...>> {
  // TODO generally one should promote the field type from all args.
  using field_type = field_t<Arg>;
  using real_type = real_t<Arg>;
};

} // end namespace Dune

#endif // DUNE_MATRIX_VECTOR_TYPES_MULTITYPEVECTOR_HH
