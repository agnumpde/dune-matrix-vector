#ifndef DUNE_MATRIX_VECTOR_MATRIXTRAITS_HH
#define DUNE_MATRIX_VECTOR_MATRIXTRAITS_HH

#include <dune/matrix-vector/traits/matrixtraits.hh>

#warning \
  This file is deprecated and might vanish soon. \
  Please use Dune::MatrixVector::Traits::MatrixTraits \
  from <dune/matrix-vector/traits/matrixtraits.hh> instead \
  or the trait utilities in namespace Dune::MatrixVector \
  to be included via <dune/matrix-vector/traits/utilities.hh>.

namespace Dune { namespace MatrixVector {

template <class T>
using MatrixTraits = Traits::MatrixTraits<T>;

}}

#endif // DUNE_MATRIX_VECTOR_MATRIXTRAITS_HH
