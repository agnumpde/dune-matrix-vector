#ifndef DUNE_MATRIX_VECTOR_SCALARTRAITS_HH
#define DUNE_MATRIX_VECTOR_SCALARTRAITS_HH

#include <dune/matrix-vector/traits/scalartraits.hh>

#warning \
  This file is deprecated and might vanish soon. \
  Please use Dune::MatrixVector::Traits::ScalarTraits \
  from <dune/matrix-vector/traits/scalartraits.hh> instead \
  or the trait utilities in namespace Dune::MatrixVector \
  to be included via <dune/matrix-vector/traits/utilities.hh>.

namespace Dune { namespace MatrixVector {

template <class T>
using ScalarTraits = Traits::ScalarTraits<T>;

}}

#endif // DUNE_MATRIX_VECTOR_SCALARTRAITS_HH
